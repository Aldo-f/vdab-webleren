<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Analyse</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css"
          integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
</head>
<body>

<div class="container">
    <div class="jumbotron">
        <h1 class="display-3">Analyse</h1>
        <p class="lead">In deze opgave is het de bedoeling een eenvoudig analyseprogramma te schrijven. Het programma
            leest alle karakters van een bestand in en toont daarna op het scherm een tabel met in de linkerkolom elk
            karakter, en in de rechterkolom het aantal keer dat dit karakter in het bestand voorkomt. Om te testen kan
            je een voorbeeldbestand downloaden.<br><br>

            Sla je PHP-pagina op als analyse.php. Test je oplossing uit door het programma verschillende bestanden te
            laten inlezen. Hieronder vind je een screenshot van de pagina wanneer voorbeeldbestand.txt werd
            ingelezen.</p>
        <hr class="my-2">
        <p>
            <?php
            $voorbeeldbestand = "voorbeeldbestand.txt";

            /*********
             * terwijl bestand niet op het einde is,
             * maak elke letter lowercase voor het tellen
             * plaats karakter in een array
             *
             * maak 2 arrays; 1 voor het karakter, een 2de voor het aantal keer dat deze voorkomt --> combineer
             * Sorteer deze alfabetisch
             *
             *
             */


            function maakArray($string)
            {
                $lowerCase = strtolower($string);
//                echo $lowerCase . "<br>";

                $array = str_split($lowerCase);
//                print_r($array);
//                echo "<br>";

                asort($array);

                $uniekeLetters = array_unique($array);
//                print_r($uniekeLetters);
//                echo "<br>";

//                echo substr_count($lowerCase, $array[0]) . "<br>"; // komt 2 keer voor

//                echo "<ul>";
                for ($i = 0; $i < sizeof($array); $i++) {
//                    echo "<li>" .
                    $array[$i]//. " komt  ";
                    ;
//                    echo substr_count($lowerCase, $array[$i]) . " keer voor</li>";
                }
//                echo "</ul>";
//                echo "<hr>";

                return $array;
            }

            function maakTabel($array)
            {

                for ($i = 0; $i < sizeof($array); $i++) {
                    $toUpper = strtoupper($array[$i]);
                    $inputArr[] = $toUpper;
                }
                $freq = array_count_values($inputArr);
                // print_r($freq);

                ksort($freq);

                echo "<br>";

                echo "<div class=\"table-responsive table-striped \"><table class=\"table\"><thead><tr><th>Karakter</th><th>#</th></tr></thead><tbody>";
                foreach ($freq as $key => $value) {
                    echo "<tr><td>$key</td><td>$value</td></tr>";
                }
                echo "</tbody></table></div>";
            }

            ?>


            <?php
            $testString = "Lotte is slimpie";

            $array = maakArray($testString);

            maakTabel($array);
            ?>

            <?php

            /****************************************************************************
             * Haal data binnen,
             * maak er een array van,
             * en stuur deze mee als array in de maakTabel() funtie
             ***************************************************************************/
            $bestandnaam = "voorbeeldbestand.txt";

            $fd = fopen($bestandnaam, "r");
            $bestand = (fread($fd, filesize($bestandnaam)));
            fclose($fd);

            $array = maakArray($bestand);

            maakTabel($array);
            ?>


        </p>
    </div>
</div>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"
        integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn"
        crossorigin="anonymous"></script>
</body>
</html>