<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Inputzin</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css"
          integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
    <style>
        /* Extra
        *************************/
        .wrapper {
            position: relative;
            height: 100vh;
        }

        .in-wrapper {
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
        }

        .jumbotron {
            padding: 3rem;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="row wrapper">
        <div class="jumbotron in-wrapper text-center">
            <h1 class="display-3">Ik google het eens voor je</h1>
            <form method="get" action="http://www.google.be/search">
                <div class="form-group">
                    <label for="">Type je zoekvraag in:</label>
                    <input type="text" style="text-align: center"
                           class="form-control" name="q" id="" aria-describedby="helpId"
                           placeholder="Wat wil je weten?" required>
                </div>
                <button type="submit" class="btn btn-primary">Zoeken met Google</button>
            </form>
        </div>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"
        integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn"
        crossorigin="anonymous"></script>
</body>
</html>