<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Inputzin</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css"
          integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
</head>
<body>
<div class="container">
    <div class="jumbotron">
        <h1 class="display-3">Hier zijn het aantal elementen dat u opvroeg</h1>
        <p>
            <?php
            /* Methode 1
            ***********************/

            //            print_r($_POST);
            //            echo "<br>";

            //Haal data binnen
            $aantal = $_POST["aantal"];
            //            echo $aantal . "<br>";

            $maanden = array("jan", "feb", "maa", "apr", "mei", "jun", "jul", "aug", "sep", "okt", "nov", "dec");

            // Wanneer kleiner dan 1, dan zal er toch 1 element getoond worden
            if ($aantal < 1) $aantal = 1;

            echo "<ol>";
            // toon het aantal maanden volgens $aantal
            for ($i = 1; $i <= $aantal; $i++) {
                if ($i <= 12) {
                    echo "<li>" . $maanden[$i - 1] . "</li>";
                }
            }
            echo "</ol>";

            /*
             * Toon volgende aantal elementen (get)
             * $i = $aantal en dan $aantal = $aantal + $aantal
             *
             */
            $i = $aantal + 1;
            $aantal += $aantal;

            echo $i . "<br>";
            echo $aantal . "<br>";

            echo "<ol>";
            for ($i; $i <= $aantal; $i++) {
                if (!($i > 12)) {
                    echo "<li>" . $maanden[$i - 1] . "</li>";
                }
            }
            echo "</ol>";

            ?>

            <a name="" id="" class="btn btn-primary"
               href="2.4-toon-elementen.php?aantal=<?php echo $aantal + $aantal ?>"
               role="button">Volgende aantal elementen</a>


        </p>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"
        integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn"
        crossorigin="anonymous"></script>
</body>
</html>