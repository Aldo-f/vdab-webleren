<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Inputzin</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css"
          integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
    <style>
        /* Extra
        *************************/
        .wrapper {
            position: relative;
            height: 100vh;
        }

        .in-wrapper {
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
        }


    </style>
</head>
<body>
<div class="container">
    <div class="wrapper">
        <div class="jumbotron in-wrapper text-center">
            <h1 class="display-3">Verwerking met get</h1>
            <p>
                <?php
                if (empty($_GET)) { ?>

            <div class="btn-group" role="group" aria-label="Basic example">
                <a href="2.3-kleuren.php?kleur=rood" role="button" class="btn btn-danger">Rood</a>
                <a href="2.3-kleuren.php?kleur=blauw" role="button" class="btn btn-primary">Blauw</a>
                <a href="2.3-kleuren.php?kleur=geel" role="button" class="btn btn-warning">Geel</a>
                <a href="2.3-kleuren.php?kleur=groen" role="button" class="btn btn-success">Groen</a>
            </div>

            <?php } else { ?>
                <div> Je koos <?php print($_GET["kleur"]) ?>.<br>
                    <a href="2.3-kleuren.php" role="button" class="btn btn-secondary">Kerekeerwere</a>
                </div>
            <?php } ?>
            </p>
        </div>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"
        integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn"
        crossorigin="anonymous"></script>
</body>
</html>