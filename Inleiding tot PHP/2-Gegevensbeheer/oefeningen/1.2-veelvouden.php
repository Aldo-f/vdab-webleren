<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Veelvouden</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css"
          integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
</head>
<body>

<div class="container">
    <div class="jumbotron">
        <h1 class="display-3">Veelvouden</h1>
        <p class="lead">
            <div class="ilc_Paragraph ilc_text_block_Standard">
        <p>Schrijf een programma dat alle veelvouden van een zeker getal <span mce_name="em"
                                                                               mce_style="font-style: italic;"
                                                                               style="font-style: italic;"
                                                                               class="Apple-style-span">$veelvoud</span>
            uitschrijft naar een bestand <span mce_name="em" mce_style="font-style: italic;" style="font-style: italic;"
                                               class="Apple-style-span">output.txt</span>, zolang de uitgeschreven
            veelvouden de waarde van <span mce_name="em" mce_style="font-style: italic;" style="font-style: italic;"
                                           class="Apple-style-span">$bovengrens</span> niet overschrijden.</p>
        <p>Bijvoorbeeld: als <span mce_name="em" mce_style="font-style: italic;" style="font-style: italic;"
                                   class="Apple-style-span">$veelvoud</span> de waarde 5 heeft en <span mce_name="em"
                                                                                                        mce_style="font-style: italic;"
                                                                                                        style="font-style: italic;"
                                                                                                        class="Apple-style-span">$bovengrens</span>
            de waarde 100, dan is de inhoud van <span mce_name="em" mce_style="font-style: italic;"
                                                      style="font-style: italic;"
                                                      class="Apple-style-span">output.txt</span>, na uitvoer van het
            programma:</p>
        <p><span style="font-family: 'Courier New', Courier;" mce_style="font-family: 'Courier New', Courier;">5 10 15 20 25 30 35 40 45 50 55 60 65 70 75  80 85 90 95 100</span>
        </p>
        <p>Als het bestand <span mce_name="em" mce_style="font-style: italic;" style="font-style: italic;"
                                 class="Apple-style-span">output.txt</span> reeds bestaat, moet het worden leeggemaakt
            alvorens er opnieuw in weggeschreven wordt, zoniet wordt het aangemaakt.</p><!--Break--></div>
    </p>
    <hr class="my-2">
    <span style="font-family: 'Courier New', Courier;" mce_style="font-family: 'Courier New', Courier;">
        <?php
        $veelvoud = 5;
        $bovengrens = 100;
        $waarde = null;

        // Open bestand
        $bestandsnaam = "output.txt";
        // in modus: w
        $fd = fopen($bestandsnaam, "w");

        // Methode 1
        if (is_writable($bestandsnaam)) {
            while ($waarde < $bovengrens) {
                $waarde = $waarde + $veelvoud;
                // toon in de php-pagina
                echo $waarde . " ";
                // schrijf weg naar bestand output.txt
                fwrite($fd, $waarde . " ");
            }
        } else {
            echo "Bestand met naam: $bestandsnaam, is niet schrijfbaar";
        }

        /* Methode VDAB
         for ($i = $veelvoud; $i <= $bovengrens; $i += $veelvoud) {
            fwrite($fd, $i . " ");
        }*/

        // Sluit bestand
        fclose($fd);
        ?>

    </span>
</div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"
        integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn"
        crossorigin="anonymous"></script>
</body>
</html>