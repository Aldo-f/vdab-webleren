<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Inputzin</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css"
          integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
</head>
<body>
<div class="container">
    <div class="jumbotron">
        <h1 class="display-3">Outputzin</h1>
        <p>
            De ingetikte zin: <br>
            <?php

             print_r($_POST);

            //Haal data binnen
            $eenZin = null;
            $eenZin = $_POST["eenZin"];

            // Toon data
            echo "<code>" . $eenZin . "</code><br><br>";

            // Tel karakters
            echo "Aantal karakters: ";
            echo strlen($eenZin) . "<br><br>";

            // Bevat hoofdletter(s)?
            // Controle met reguliere expresie, gebruik van "/" voor het teken "[" correct binnen te krijgen
            echo "Zin bevat hoodletters? ";
            $patroon = "/[A-Z]/";
            if (preg_match($patroon, $eenZin) == true) echo "Ja";
            else echo "Nee";

            // Methode 2 voor controle hoofdletter(s)
            // Controle of de kleineletterszin gelijk is aan de normale zin; indien waar bevat de zin geen hoofdletter(s)
            echo "<br><br>Zin bevat hoodletters? ";
            $kleineletterszin = strtolower($eenZin);
            if ($kleineletterszin == $eenZin) $hoofd = "Nee";
            else $hoofd = "Ja";
            echo $hoofd;


            ?>
        </p>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"
        integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn"
        crossorigin="anonymous"></script>
</body>
</html>