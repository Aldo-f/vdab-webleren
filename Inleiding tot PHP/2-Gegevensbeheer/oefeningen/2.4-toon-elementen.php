<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Inputzin</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css"
          integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
</head>
<body>
<div class="container">
    <div class="jumbotron">
        <h1 class="display-3">Hier zijn het aantal elementen dat u opvroeg</h1>
        <p>
            <?php
            $maanden = array("jan", "feb", "maa", "apr", "mei", "jun", "jul", "aug", "sep", "okt", "nov", "dec");

            // Controleer of het begin van de lijst als parameter aan de URL werd toegevoegd.
            if (!isset($_GET['begin'])) $begin = 0;
            else $begin = $_GET['begin'];

            // Controleer of het aantal elementen als parameter aan de URL werd toegevoegd.
            if (!isset($_GET['aantal'])) $aantal = $_POST['aantal'];
            else $aantal = $_GET['aantal'];

            // Controleer of de bezoeker niet een te hoog of te laag aantal ingegeven heeft
            if ($aantal > count($maanden)) $aantal = count($maanden);
            else if ($aantal < 1) $aantal = 1;

            // Schrijf de juiste elementen uit
            $teller = $begin;
            while ($teller < $begin + $aantal && $teller < count($maanden)) {
                print("$teller. $maanden[$teller]<br>");
                $teller++;
            }
            ?>
        </p>
        <p>
            <?php if ($begin + $aantal < count($maanden)) { ?>
                <a  class="btn btn-primary" href="2.4-toon-elementen.php?begin=<?php print($begin + $aantal); ?>&aantal=<?php print($aantal); ?>">
                    Vraag volgende <?php print($aantal); ?> elementen op
                </a>
            <?php } ?>
        </p>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"
        integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn"
        crossorigin="anonymous"></script>
</body>
</html>