<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Klad</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css"
          integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
</head>
<body>

<div class="container">
    <div class="jumbotron">
        <h1 class="display-3">Klad gegevensbeheer</h1>
        <p class="lead">Hier komen er kladberichtjes ivm gegevensbeheer</p>
        <hr class="my-2">
        <p class="lead">
            Welke van onderstaande uitspraken is fout?
        <ul>
            <li>Een file descriptor bevat de bestandsnaam van het bestand waar deze naar verwijst.</li>
        </ul>
        <p>De functie waarmee een bestand in PHP kan geopend worden is fopen(). De syntaxis is de volgende:</br>

            <code>$fd = fopen(bestand, mode);</code></p>
        <br>
        <div class="table table-bordered table-responsive table-striped">
            <table class="table table-bordered">
                <tbody>
                <tr valign="top">
                    <th class="ilc_table_cell_HorHeaderTableBlauwH" width="" style="padding: 2px;border: solid 1px;">
                        <div class="ilc_Paragraph ilc_text_block_TableContent">Modus<!--Break--></div>
                    </th>
                    <th class="ilc_table_cell_HorHeaderTableBlauwH" width="" style="padding: 2px;border: solid 1px;">
                        <div class="ilc_Paragraph ilc_text_block_TableContent">Beschrijving<!--Break--></div>
                    </th>
                </tr>
                <tr valign="top">
                    <td class="ilc_table_cell_HorHeaderTableBlauwC1" width="" style="padding: 2px;border: solid 1px;">
                        <div class="ilc_Paragraph ilc_text_block_TableContent">r<!--Break--></div>
                    </td>
                    <td class="ilc_table_cell_HorHeaderTableBlauwC1" width="" style="padding: 2px;border: solid 1px;">
                        <div class="ilc_Paragraph ilc_text_block_TableContent">Het bestand kan alleen gelezen worden. De
                            bestandspointer wordt aan het begin van het bestand geplaatst. De letter 'r' komt van "read"
                            (lezen).<!--Break--></div>
                    </td>
                </tr>
                <tr valign="top">
                    <td class="ilc_table_cell_HorHeaderTableBlauwC2" width="" style="padding: 2px;border: solid 1px;">
                        <div class="ilc_Paragraph ilc_text_block_TableContent">r+<!--Break--></div>
                    </td>
                    <td class="ilc_table_cell_HorHeaderTableBlauwC2" width="" style="padding: 2px;border: solid 1px;">
                        <div class="ilc_Paragraph ilc_text_block_TableContent">Het bestand kan gelezen en beschreven
                            worden. De bestandspointer wordt aan het begin van het bestand geplaatst.<!--Break--></div>
                    </td>
                </tr>
                <tr valign="top">
                    <td class="ilc_table_cell_HorHeaderTableBlauwC1" width="" style="padding: 2px;border: solid 1px;">
                        <div class="ilc_Paragraph ilc_text_block_TableContent">w<!--Break--></div>
                    </td>
                    <td class="ilc_table_cell_HorHeaderTableBlauwC1" width="" style="padding: 2px;border: solid 1px;">
                        <div class="ilc_Paragraph ilc_text_block_TableContent">Het&nbsp;bestand kan alleen beschreven
                            worden. De bestandspointer wordt aan het begin van het bestand geplaatst. Het bestand zelf
                            wordt volledig leeggemaakt. Bestaat het bestand nog niet, dan wordt het nu aangemaakt. De
                            letter 'w' komt van "write" (schrijven).<!--Break--></div>
                    </td>
                </tr>
                <tr valign="top">
                    <td class="ilc_table_cell_HorHeaderTableBlauwC2" width="" style="padding: 2px;border: solid 1px;">
                        <div class="ilc_Paragraph ilc_text_block_TableContent">w+<!--Break--></div>
                    </td>
                    <td class="ilc_table_cell_HorHeaderTableBlauwC2" width="" style="padding: 2px;border: solid 1px;">
                        <div class="ilc_Paragraph ilc_text_block_TableContent">Het bestand kan gelezen en beschreven
                            worden. De bestandspointer wordt aan het begin van het bestand geplaatst. Het bestand zelf
                            wordt volledig leeggemaakt.<!--Break--></div>
                    </td>
                </tr>
                <tr valign="top">
                    <td class="ilc_table_cell_HorHeaderTableBlauwC1" width="" style="padding: 2px;border: solid 1px;">
                        <div class="ilc_Paragraph ilc_text_block_TableContent">a<!--Break--></div>
                    </td>
                    <td class="ilc_table_cell_HorHeaderTableBlauwC1" width="" style="padding: 2px;border: solid 1px;">
                        <div class="ilc_Paragraph ilc_text_block_TableContent">Het bestand kan alleen beschreven worden.
                            De bestandspointer wordt aan het einde van het bestand geplaatst. Als het bestand niet
                            bestaat wordt het aangemaakt. De letter 'a' komt van "append" (toevoegen).<!--Break--></div>
                    </td>
                </tr>
                <tr valign="top">
                    <td class="ilc_table_cell_HorHeaderTableBlauwC2" width="" style="padding: 2px;border: solid 1px;">
                        <div class="ilc_Paragraph ilc_text_block_TableContent">a+<!--Break--></div>
                    </td>
                    <td class="ilc_table_cell_HorHeaderTableBlauwC2" width="" style="padding: 2px;border: solid 1px;">
                        <div class="ilc_Paragraph ilc_text_block_TableContent">Het bestand kan gelezen en beschreven
                            worden. De bestandspointer wordt aan het einde van het bestand geplaatst. Als het bestand
                            niet bestaat wordt het aangemaakt.<!--Break--></div>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
        </p>
        <p>
            Als variabelenaam voor de file descriptor kiest men dikwijls <code>$fd</code>.
        </p>
        <p>Volgend kort voorbeeld toont aan hoe je het bestand mijndata.txt kan openen met leestoegang en opnieuw
            sluiten:<br>

            <code>$fd = fopen("mijndata.txt", "r");<br>
                fclose($fd);</code></p>
        <p>
            <code>$inhoud = fread(filedescriptor, filesize(bestandsnaam));<br><br>
                $fd = fopen(bestandsnaam, mode)<br>
                while (!feof($fd)) $regel = fgets($fd);
            </code>
            De functie <code>fgetc()</code> is analoog aan de functie <code>fgets()</code>, met dat verschil dat <code>fgetc()</code>
            slechts één teken per keer inleest.
        </p>
        <hr>
        <p>
            wegschrijven in een bestand<br>
            <code> fwrite(filedescriptor, string);</code><br>
            Is schrijfbaar?<br>
            <code>if (is_writable('mijnbestand.txt')) {
                ...
                }
            </code>
        </p>
        <p>
            <?php
            $zin = "Dit is een tekst die in het bestand moet komen te staan.";
            $bestandsnaam = "beschreven.txt";
            $fd = fopen($bestandsnaam, "r");
            //        $fd = fopen($bestandsnaam, "w");
            if (!is_writable($bestandsnaam)) {
                fwrite($fd, $zin);
            } else {
                echo "Bestand met naam: $bestandsnaam, is niet schrijfbaar";
            }
            fclose($fd);
            ?>
        </p>
        <hr>
        <p>
            <div>
        <p>Het is mogelijk om het <strong>formulier en de verwerking van het formulier op één pagina te
                integreren</strong> via een if...else opdracht. Hierdoor is het dus niet nodig om 2 pagina's aan te
            maken voor elk formulier dat je wil verwerken. De structuur van dergelijke pagina ziet er als volgt uit:</p>
        <p><font color="#800000" face="Courier New, Courier">&lt;?php if (empty ($_POST)) {</font></p>
        <p><font color="#800000" face="Courier New, Courier">// formulier</font></p>
        <p><font color="#800000" face="Courier New, Courier">}</font></p>
        <p><font color="#800000" face="Courier New, Courier">else {</font></p>
        <p><font color="#800000" face="Courier New, Courier">// verwerking formulier</font></p>
        <p><font color="#800000" face="Courier New, Courier">} ?&gt;</font></p>
        <p>Als de array $_POST leeg is tonen we het formulier aan de gebruiker. Het attribuut <em>action</em> van het
            formulier is nu de naam van de pagina waarop het formulier staat. Als de gebruiker op de knop verzenden
            klikt roept het formulier dezelfde pagina opnieuw op. De array $_POST zal nu niet meer leeg zijn waardoor
            het formulier niet opnieuw wordt getoond. Het else-gedeelte van de pagina zal nu getoond worden. Hierin zal
            de verwerking van het formulier plaatsvinden.</p></div>
    <span style="font-family: Courier New,Courier; color: rgb(128, 0, 0);"
          mce_style="font-family: Courier New,Courier; color: #800000;"><span style="color: rgb(0, 0, 255);"
                                                                              mce_style="color: #0000ff;">&lt;form action="</span>&lt;?php print($_SERVER["PHP_SELF"]);  ?&gt;<span
                style="color: rgb(0, 0, 255);" mce_style="color: #0000ff;">" method="post"&gt;</span></span>
    </p>


</div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"
        integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn"
        crossorigin="anonymous"></script>
</body>
</html>