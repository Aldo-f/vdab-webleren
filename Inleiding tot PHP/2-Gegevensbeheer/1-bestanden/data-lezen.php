<<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Data lezen</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css"
          integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
</head>
<body>

<?php $bestandnaam = "informatie.txt"; ?>


<div class="container">
    <div class="jumbotron">
        <h1 class="display-3">Bestand lezen</h1>
        <p class="lead">Bestand in één keer inlezen</p>
        <p class="lead">
            <?php
            $fd = fopen($bestandnaam, "r");
            // print de size van het bestand (of de helft)
            print (fread($fd, filesize($bestandnaam) / 2));
            fclose($fd);
            ?>
        </p>
        <p class="lead">Regel voor regel inlezen</p>
        <p class="lead">
            <?php
            $fd = fopen($bestandnaam, "r");
            //terwijl bestand niet geïndigd is, print lijn
            while (!feof($fd)) print(fgets($fd));
            fclose($fd);
            ?>
        </p>
        <p class="lead">Karakter per karakter inlezen</p>
        <p class="lead">
            <?php
            $fd = fopen($bestandnaam, "r");
            //terwijl bestand niet geïndigd is, print karakter
            while (!feof($fd)) print (fgetc($fd));
            fclose($fd);
            ?>
        </p>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"
        integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn"
        crossorigin="anonymous"></script>
</body>
</html>