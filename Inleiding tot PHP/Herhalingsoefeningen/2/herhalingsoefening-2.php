<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Herhalingsoefening 2</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css"
          integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
    <style>
        .vandaag {
            background-color: #FFFF00;
        }

        .jumbotron {
            margin-top: 2rem;
        }
    </style>
</head>
<body>
<?php
$locale = "nl_BE.utf8";
setlocale(LC_ALL, $locale);
$huidigeDatum = date("d-m-Y");
$huidigeDatumInNl = strftime("%e %B %Y", strtotime($huidigeDatum));
$huidigeDagInNl = strftime("%A", strtotime(date("d-m-Y")));

// Huidige datum
$huidigeDag = date("d");
$huidigeMaand = date("m");
$huidigJaar = date("Y");

// Testen
$testMaand = 4;
$testJaar = 2018;

// array van nederlandstalige maandnamen (start bij nr 1)
$maandNaam = array(1 => "januari", "februari", "maart", "april", "mei", "juni", "juli", "augustus", "september", "oktober", "november", "december");

// array van nederlendstalige dagnamen (eerste start bij 1)
$dagNaam = array(1 => "ma", "di", "wo", "do", "vr", "za", "zo");

/* Aantal dagen in huidige maand?
****************************************************/
function dagenInMaand($maand, $jaar)
{
    // Methode 1
    $dagenInMaand = cal_days_in_month(CAL_GREGORIAN, $maand, $jaar);

    // Methode 2
    // $tijdstipInMaand = mktime(0, 0, 0, $maand, 1, $jaar);
    // $dagenInMaand = date("t", $tijdstipInMaand);

    return $dagenInMaand;
}

/* Aantal weken in huidige maand?
 ***************************************************/
function wekenInMaand($maand, $jaar)
{
    if (null == ($jaar)) {
        $jaar = date("Y", time());
    }

    if (null == ($maand)) {
        $maand = date("m", time());
    }

    $dagenInMaand = dagenInMaand($maand, $jaar);

    // modulo 7 van 28 = 0 + intval (geheel getal zonder komma) 28/7 = 4 --> 4 weken
    //(String mood = inProfit() ? "happy" : "sad";)
    $aantalWeken = ($dagenInMaand % 7 == 0 ? 0 : 1) + intval($dagenInMaand / 7);

    $maandEerste = date('N', strtotime($jaar . '-' . $maand . '-01'));

    $maandLaatste = date('N', strtotime($jaar . '-' . $maand . '-' . $dagenInMaand));

    if ($maandLaatste < $maandEerste) {
        $aantalWeken++;
    }
    return $aantalWeken;
}

/* Maak de kalender
****************************************************/
function maakKalender($maand, $jaar)
{
    echo ' <thead>';
    maakHeader();
    echo '</thead><tbody>';
    maakTabel($maand, $jaar);
    echo "</tbody>";
}

function maakHeader()
{
    global $dagNaam;
    echo "<tr>";
    // Toon de dagen van de week zoals eerder gemaakt
    foreach ($dagNaam as $key => $value) {
        echo("<th>$value</th>");
    }
    echo "<tr>";
}

function maakTabel($maand, $jaar)
{
    $dagenInMaand = dagenInMaand($maand, $jaar);

    // Voor controle vandaag
    global $huidigeDag;
    global $huidigeMaand;
    global $huidigJaar;

    /* rij voor week 1 */
    echo "<tr>";

    // - 1 voor eerste dag van de week als maandag ipv zondag
    $eersteDagVanDeMaand = date('N', strtotime($jaar . '-' . $maand . '-01')) - 1;

    $dagTeller = 0;
    $dagenInDezeWeek = 1;

    /* lege <td> tot wnr de 1ste week begint */
    for ($x = 0; $x < $eersteDagVanDeMaand; $x++):
        echo "<td></td>";
        $dagenInDezeWeek++;
    endfor;

    /* Noteer alle dagen van de maand */
    for ($noteerDag = 1; $noteerDag <= $dagenInMaand; $noteerDag++):
        echo "<td";

        // noteer huidige datum
        if ($noteerDag == $huidigeDag && $maand == $huidigeMaand && $jaar == $huidigJaar) {
            echo " class='vandaag'>";
        } else {
            echo ">";
        }

        /* noteer de dag */
        echo $noteerDag . "</td>";

        if ($eersteDagVanDeMaand == 6):
            echo "</tr>";
            if (($dagTeller + 1) != $dagenInMaand):
                echo "<tr>";
            endif;
            $eersteDagVanDeMaand = -1;
            $dagenInDezeWeek = 0;
        endif;
        $dagenInDezeWeek++;
        $eersteDagVanDeMaand++;
        $dagTeller++;
    endfor;

    /* vul resterende rij aan met lege velden */
    if ($dagenInDezeWeek < 7):
        for ($x = 0; $x <= (7 - $dagenInDezeWeek); $x++):
            echo "<td></td>";
        endfor;
    endif;

    /* laatste rij sluiten */
    echo "</tr>";
}

?>

<div class="container">
    <div class="jumbotron">
        <h1 class="display-3">Opdracht 2</h1>
        <h2 class="display-5">Een kalender voor de maand <?php echo $maandNaam[$huidigeMaand]; ?></h2>
        <p class="lead">
            We zijn vandaag <?php echo $huidigeDagInNl . ", " . $huidigeDatumInNl ?>. <br>
            In deze maand zijn er <?php echo wekenInMaand($huidigeMaand, $huidigJaar); ?> weken.
        </p>
        <hr>
        <p>Kalender voor <?php echo $maandNaam[$huidigeMaand] . " " . $huidigJaar; ?> (de huidige maand)</p>
        <div class="table table-striped table-responsive table-bordered">
            <table class="table">
                <?php maakKalender($huidigeMaand, $huidigJaar) ?>
            </table>
        </div>
        <p>Kalender voor <?php echo $maandNaam[$testMaand] . " " . $testJaar ?> (als test maand)<br>
            In deze maand zijn er <?php echo wekenInMaand($testMaand, $testJaar); ?> weken.<br>
            U kan dit aanpassen op lijn 35 en 36: <code>$testMaand = <?php echo $testMaand ?>;
                $testJaar = <?php echo $testJaar ?>;</code>
        </p>
        <div class="table table-striped table-responsive table-bordered">
            <table class="table">
                <?php maakKalender($testMaand, $testJaar) ?>
            </table>
        </div>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"
        integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn"
        crossorigin="anonymous"></script>
</body>
</html>