<!DOCTYPE html>
<html lang="">
<?php
$soorten = array("Harten", "Koeken", "Klaveren", "Schoppen");
$waarden = array(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, "Boer", "Dame", "Heer");
$kaarten = array();

for ($i = 0; $i < count($soorten); $i++) {
    for ($j = 0; $j < count($waarden); $j++) {
        array_push($kaarten, "$soorten[$i] $waarden[$j]");
    }
}

//        print_r ($kaarten);
// echo "<br>";
// echo sizeof($kaarten);
// echo "<br>";
$randomKaartNr = rand(0, sizeof($kaarten) - 1);
//        echo $randomKaartNr;
//        echo "<br>";

?>

<head>
    <meta charset="utf-8">
    <meta http-equiv="refresh" content="5">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>
        <?php echo $randomKaartNr ?>
    </title>

    <!-- Bootstrap CSS -->
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
          crossorigin="anonymous">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn t work if you view the page via file:// -->
    <!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
    <style>
        body,
        html {
            height: 100%;
        }
    </style>
</head>

<body>

    <div class="container h-100">
        <div class="row h-100 justify-content-center align-items-center">
            <div class="row bg-faded jumbotron">
                <h3 class="display-6">
                    De getrokken kaart is:<br>
                    <?php echo $kaarten[$randomKaartNr]; ?>
                </h3>
            </div>
        </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
            integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
            integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
            crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
            integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
            crossorigin="anonymous"></script>
</body>
</html>