<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Funtie gemiddelde</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css"
          integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
</head>
<body>
<div class="container">


    <div class="jumbotron center">
        <h1 class="display-3">Funtie gemiddelde</h1>

        <?php
        function gemiddelde($array)
        {
            $som = array_sum($array);
            $aantalWaarden = count($array);

            $gemiddelde = $som / $aantalWaarden;

            return $gemiddelde;
        }

        $array1 = array(1, 2, 3, 4, 5);
        $array2 = array(10, 5, 3);
        $array3 = array(3, 4, 5, 2, 5, 1, 2, 1, 0, 2);


        ?>

        <p class="lead">
            <?php
            echo gemiddelde($array1) . "<br>";
            echo gemiddelde($array2) . "<br>";
            echo gemiddelde($array3) . "<br>";
            ?>
        </p>
    </div>

</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"
        integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn"
        crossorigin="anonymous"></script>
</body>
</html>