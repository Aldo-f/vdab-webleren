<!DOCTYPE html>
<html lang="">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Title Page</title>

    <!-- Bootstrap CSS -->
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn t work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
<?php
/* Eigen methode
***********************************/
$vandaag = date("d-m-Y");
//echo "We zijn vandaag: " . $vandaag . "<br>";
$nieuwjaar = date("Y") + 1;
//echo $nieuwjaar . " is het volgende nieuwjaar<br>";
$aantalDagenInJaar = date("z", mktime(0, 0, 0, 12, 31, date("Y"))) + 1;
//echo $aantalDagenInJaar . " dagen in huidige jaar<br>";
$aantalDagenReedsGepaseerd = date("z");
//echo "Er zijn reeds " . $aantalDagenReedsGepaseerd . " gepaseerd dit jaar<br>";
$aantalDagenTotNieuwjaar = $aantalDagenInJaar - $aantalDagenReedsGepaseerd;
//echo "Binnen " . $aantalDagenTotNieuwjaar . " dagen is het nieuwjaar";
?>

<div class="container">
    <div class="row justify-content-md-center">
        <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
            <div class="jumbotron">
                <h1 class="display-3">Nieuwjaar <?php echo $nieuwjaar ?></h1>
                <p class="lead">Nog even geduld om de champagne te ontkurken, binnen
                    <?php echo $aantalDagenTotNieuwjaar ?> dagen is het zo ver.
                </p>
            </div>
        </div>
    </div>
</div>
<?php
/* Methode VDAB
****************************************/
$jaartal = date("Y");
// We bepalen welk jaartal het momenteel is.

$volgend_jaar = $jaartal + 1;
// We bepalen welk jaartal het volgend jaar is.

$nieuwjaar = mktime(0, 0, 0, 1, 1, $volgend_jaar);
// We bepalen hoeveel tijd er verlopen is tussen 1 januari 1970 en 1 januari volgend jaar (in seconden).

$verschil = $nieuwjaar - time();
// We maken het verschil tussen 1 januari volgend jaar en het huidige tijdstip (in seconden).

$verschil = $verschil / 60 / 60 / 24;
// We zetten het verschil in seconden om in het verschil in dagen.

$verschil = ceil($verschil);
// We ronden naar boven af.
?>
<div class="container">
    <div class="row justify-content-md-center">
        <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
            <div class="jumbotron">
                <h1 class="display-3">Nieuwjaar <?php echo $volgend_jaar ?></h1>
                <p class="lead"><?php print("Binnen " . $verschil . " dagen laten we de champagne knallen!!!"); ?>
                </p>
            </div>
        </div>
    </div>
</div>


<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
        integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
        crossorigin="anonymous"></script>
</body>

</html>
