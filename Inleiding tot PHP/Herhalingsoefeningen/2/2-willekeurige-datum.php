<!DOCTYPE html>
<html lang="">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Title Page</title>

    <!-- Bootstrap CSS -->
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn t work if you view the page via file:// -->
    <!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
</head>

<body>
    <div class="container">


        <h1 class="text-center">Willekeurige datums</h1>
        <?php
//            echo $willekeurigeDatum = date('j/m/Y', rand());
        //
//            $start = 2010;
//            $einde = 2020;
        //
//            function datum($jaar, $maanden = array(12)){
        //
//            print_r ($maanden[5]);
//            }
        //
//            datum($start, 12);
        //
        //
        $beginjaar = 2010;
        $eindjaar = 2020;

        $test = 20;

        // We maken onze functie aan
        // Als input krijgt de functie een jaartal en de mogelijke maanden, voorgesteld als array die het nummer van elke maand bevat
        // Als er geen array wordt opgegeven is elke maand mogelijk
        function datum($jaar, $maanden = array(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12))
        {

            // We maken een array met de nederlandstalige benamingen van de maanden (denk er aan dat het eerste element index 0 heeft
            $maandnaam = array("januari", "februari", "maart", "april", "mei", "juni", "juli", "augustus", "september", "oktober", "november", "december");

            // We bepalen een willekeurig getal dat minimaal 1 is en maximaal het aantal elementen in de array met maanden
            $maandnummer = rand(1, count($maanden)); // 1

            // We zoeken in de array maanden het nummer van de maand op die we zoeken
            // Hou er rekening mee dat de index van deze maand één minder is dan de willekeurige waarde die we gevonden hebben
            $maand = $maanden[$maandnummer - 1]; // januari
//    echo $maand . " = nummer van de maand<br>";

            // We zoeken de benaming van de maand op
            // Hou er weer rekening mee dat de index van de maand 1 minder is dan het nummer van de maand
            // Bijvoorbeeld juli heeft index 6
            $benaming_maand = $maandnaam[$maand - 1];

            // We maken een tijdstip aan in de maand die we willekeurig bepaald hebben
            $tijdstip_in_maand = mktime(0, 0, 0, $maand, 1, $jaar);
//    echo $tijdstip_in_maand . " <br>";

            // We zoeken op hoeveel dagen er in deze maand zitten
            $aantal_dagen_in_maand = date('t', $tijdstip_in_maand); // t = 1-31 of 1-30 of 1-29 of 1-28
//    echo $aantal_dagen_in_maand . "<br>";

            // We kiezen een willekeurige dag in de maand
            $dag = rand(1, $aantal_dagen_in_maand);

            // We schrijven het resultaat naar het scherm
            print("$dag $benaming_maand $jaar<br>");
        }

        // We zoeken eerst in 11 opeenvolgende jaren een willekeurige datum
        // Omdat er geen beperking is voor de maanden moet er maar één argument worden doorgegeven
        for ($i = 2010; $i <= 2020; $i++) {
            datum($i);
        }

        print("<br>");

        // We zoeken vervolgens in 11 opeenvolgende jaren een willekeurige datum in juli of augustus
        // We geven nu wel een array door met de nummers van de maanden juli en augustus
        for ($i = 2010; $i <= 2020; $i++) {
            datum($i, array(7, 8));
        }

        ?>

    </div>

    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</body>

</html>
