<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <title>Minimum Bootstrap HTML Skeleton</title>
    <!--  -->
    <style>
        .jumbotron {
            margin-bottom: 0;
        }

    </style>

</head>

<body>
    <div class="jumbotron">
        <p class="lead">
            <?php
            
            $aantal = 5; //standaard aantal 
            $tekst = "Eerste variabele, als test";
               
            //bestaat $aantal al?             
            isset($aantal);
            unset($aantal);
                        
            $aantal = 20;
                
            echo($aantal);
            echo("</br>");
            echo("Het aantal was dus $aantal");
            echo("</br>");
            echo('De variabele voor aantal is $aantal</br>'); 
            echo("Oftewel \$aantal (zie <code>deze php code</code> om het verschil te zien)</br>"); 
            print("$tekst </br>");
            print("Hij zei \"Het is koud vandaag\".</br></br>");   
            
            $getal = "40";
            $totaal = $getal + 60;
            $aantalcijfers = strlen($totaal);
            echo("Het totaal van de tekst 40 en het getal 60 is $totaal, de waarde \$totaal (dus die $totaal) bestaat uit $aantalcijfers cijfers.</br>");
            
            $typeAantal= gettype($aantal);
            print("\$aantal is een $typeAantal". " </br>"); // Een "." gebruiken noemt een "concatenatie" 
            
            $typeGetal= gettype($getal);
            print("\$getal is een $typeGetal, waar toch een som van genomen kan worden door op te tellen met een cijfer" . "</br></br>");
            
            $var1 = "Webleren ";
            $var2 = "is fun";
            print($var1 . $var2 . "</br><br>");
                
            $var1 = "40";
            $var2 = "60";
            print($var1+$var2 . "</br>");
            
            define("BTW", 0.21, true); //hoofdletterONgevoelige BTW
            $totaalMetBtw = $totaal + $totaal * BTW;
            print("$totaal met BTW komt overeen met $totaalMetBtw");
                
            echo("</br>");
            print("2 tot de 3de macht is ". pow(2,3) . "<br>");
            print("De vierkantswortel van 25 is ". sqrt(25). "<br>");
            print( 27%-4 . "<br><br>");
            
            
            
            $basis = 5;
            $hoogte = 3;
            print("Wat is de oppervlakte van een driehoek met een basis van $basis en een hoogte van $hoogte?<br>");
            
            $oppervlakte1 = $basis * $hoogte / 2;
            
            $oppervlakte2 = 1; //initaliseren van een integer
            $oppervlakte2 *= $basis;
            $oppervlakte2 *= $hoogte;
            $oppervlakte2 /= 2;
            print("Methode 1 komt uit op $oppervlakte1<br>Methode 2 geeft als oplossing: ". $oppervlakte2 . "<br><br>");
            
            $string = "Het aantal is ";
            $integer = 5;

            $samen = $string . $integer;
            print("\$samen is een ". gettype($samen) . "<br>");
            
            /* Operatoren */
            $vijf = 5;
            $tien = 10;
            print($vijf != $tien);            
            echo("<br>");
            
            $var1 = true;
            $var2 = true;
            $var3 = true;
            
            echo($var1 && $var2 && $var3);                 
            echo("<br>");
            echo("<br>");

            /* oefeningen */
            $varA = true;
            $varB = false;
            
            echo("<br>1) ");
            echo($varA || !$varB); //true or not false
            
            echo("<br>2) ");
            echo($varA && !$varB); //true and not false
            
            echo("<br>3) ");
            echo(!$varA || $varB); //not true or false
            
            echo("<br>4) ");
            echo(!$varA && $varB); //not true and false
            
            $a = 10;
            echo("<br>");
            print($a++);
            echo("<br>");
            print(++$a);         
            echo("<br>");
        
            
            /* Programmacontructies */ 
            // if (uitdrukking) opdracht; 
            $bedrag = 6;
            $prijsTicket = 8;
            if ($bedrag > $prijsTicket) {                
                echo("<p>Je hebt genoeg geld voor een ticketje voor de bioscoop!</p>");
            }
            else {
                $verschil = $prijsTicket - $bedrag; 
                echo("Je hebt niet genoeg geld voor een ticketje, je hebt €". $verschil . " te weinig.");
            }
            
            // if (uitdrukking1) opdrachtA;
            // elseif (uitdrukking2) opdrachtB;
            // elseif (uitdrukking3) opdrachtC;
            // else opdrachtD;
            
            
            //Welke dag van de week zijn we? 
            $dag = date("N");
            echo("<br><br>Het is vandaag ");
              
            if ($dag == 1) print("maan");
            elseif ($dag == 2) print("dins");
            elseif ($dag == 3) print("woens");
            elseif ($dag == 4) print("donder");
            elseif ($dag == 5) print("vrij");
            elseif ($dag == 6) print("zater");
            elseif ($dag == 7) print("zon");
            else print("een onbekende ");
            
            echo("dag ");
            
            // Welk random getal is groter? 
            $getal1 = rand();
            $getal2 = rand();
//            $getal1 = 5;
//            $getal2 = 5;
            
            echo("<br><br>Getal1 is ");
            
            if($getal1 > $getal2) print("groter dan ");
            elseif($getal1 < $getal2) print("kleiner dan ");
            else print("gelijk aan ");
            
            echo("getal2.<br><br>");
            
            /*De Switch contructie
            *********************
            switch ($variabele) {
            case "waarde1":
            { Opdrachten; }
            break;
            case "waarde2":
            { Opdrachten; }
            break;
            ...
            default:
            { Opdrachten; }
            }
            ************************/
                
            
            $budget = 10;
            print("Wat kan je kopen met " . $budget . " euro:<br />");
            switch($budget) {
            case($budget >= 20):{
            print("een DVD of ");
            }
            case($budget >= 10):{
            print("een boek of ");
            }
            case($budget >= 5):{
            print("een stripverhaal of ");
            }
            case($budget >= 2):{
            print("een tijdschrift.");
            }
            break; /* Deze break plaatsen we zodat de default niet uitgevoerd zal worden tenzij
            het budget kleiner is dan 2. */
            default:{
            print("niets");
            }
            }
            
            /* Kleur switch */ 
            $kleur = "BLAuW";
            // fix voor hoofdletters 
            $kleur = strtolower($kleur);
            $kleur = ucfirst($kleur);  
        
            echo("<br><br>");
            
            switch($kleur) {
                    case($kleur == "Blauw"):{
                        echo("Blauw is mooie kleur");
                        break;
                    }
                       case($kleur == "Groen"):{
                        echo("Groen is niet mijn favoriete kleur");
                        break;
                    }
                       case($kleur == "Rood"):{
                        echo("Rood is de kleur van de liefde");
                        break;
                    }
                default:{
                    echo("Deze kleur (" . $kleur. ") heeft ook zijn charme.");
                }
            }
            
            echo("<br><br>");
            /* De (do) while-constructie 
            while (uitdrukking) {
            opdracht1;
            opdracht2;
            ...
            }
            
            do {
            opdracht1;
            opdracht2;
            ...
            }
            while(uitdrukking);
            **********************/
                        
            do { 
                print("Deze tekst verschijnt enkel bij een do ... while lus."); 
            }
            while (1 == 2);

            while (1 == 2) { 
                print("Deze tekst zal niet op het scherm verschijnen"); 
            }
            
            
            echo("<br><br>");
            $teller = 1;
            while($teller <= 100 ){
                print("$teller ");
                $teller = $teller+1;
            }           
            
            echo("<br><br>");
            
            /* Fibonaccireeks 
            een reeks getallen die begint met 0 en 1, en waarbij elk daaropvolgend getal de som is van de twee vorige. We krijgen dus volgende reeks:

            0 1 1 2 3 5 8 13 21 34 ...

            Schrijf een programma dat de opeenvolgende getallen uit de Fibonaccireeks uitschrijft zolang het laatst uitgeschreven getal kleiner is dan 1.000.000.
            */
            
            $getal1 = 0;
            $getal2 = 1;
            print("0 1 ");
            $som = $getal1 + $getal2;
            while ($som < 1000000) {
                print("$som ");
                $getal1 = $getal2;
                $getal2 = $som;
                $som = $getal1 + $getal2;
            }
            echo("<br><br>");
            
            /* For-lus 
            Een for-lus heeft de volgende structuur:
            for (initialisatie-uitdrukking; lusvoorwaarde; luseinde-uitdrukking) opdracht
            **************************/
            $stopwaarde = 15;
            for ($teller = 1; ;$teller++) {
                print("$teller ");
                if ($teller == $stopwaarde) break;
            }
            
            $teller= 1;
            for($teller = 0; $teller <= 100; $teller++){
                print("$teller ");
            }
            
            echo("<br><br>");
            
            /* break/continue 
            Het statement break onderbreekt een lus (while, for) of een voorwaardelijk statement (if ... else, switch) en gaat verder met de uitvoering van het script vanaf het eerste statement dat volgt op de sluitaccolade.
            Het statement continue slaat de rest van de lusdoorloop over, maar gaat verder vanaf het begin van de volgende lusdoorloop. Het breekt de lus dus niet af, maar slaat gewoon een deel van de statements over. We illustreren dit in onderstaand voorbeeld. 
            ***************************************/ 
            $teller = 0;

            while(true) {

            $teller++;
            if ($teller > 10) break; //gaat uit de lus bij >10
                print($teller . " ");
            }
            
            echo("<br>");
            
            $teller = 0;

            while ($teller < 10) {
                $teller++;
                if ($teller == 6) continue; // print van 6 gaat niet door, maar blijft in lus
                print($teller . " ");
            }
            
            echo("<br><br>");
            
            //Dit toont alle oneven getallen tot en met 10
            // oplossing?: 2 4 6 8 10
            $teller = 0;
            
            while (true) {
                $teller++;
                if ($teller % 2 == 1) continue;
                if ($teller > 10) break;
                echo ($teller . " ");
            }
            
            echo('<hr> <br>');
            $getal = 15.98461;
            $afronding = 2;
            $afgerondGetal = round($getal, $afronding); 
            print('Het getal '. $getal .'  is afgerond op '. $afronding . ' cijfers, komt overeen met ' . $afgerondGetal . '<br><br>');

            
            $getalBinair = decbin($getal);
            echo('Het getal ' . $getal .' in zijn binaire vorm is ' . $getalBinair . "<br>Zoals u ziet worden de komma's niet gebruikt bij het binair getal, deze worden gewoon weggelaten<hr><br>");
            
            /* eigen functies maken */ 
            
            function BTW($prijs, $btw = 21) {
                $prijs_incl = $prijs * (1 + $btw/100);
                return $prijs_incl;
            }
            $te_betalen = BTW(100); //Als de BTW niet gespecificeerd is, dan word de standaard 21% gebruikt         
//            $te_betalen = BTW(100, 6); // Zoniet, dus met optionele waarde, wordt deze gebruikt
            print ("$te_betalen");
            
         
            echo '<br><hr><br>';
            
            function evenLang($string1, $string2 ){
                $lengte1 = strlen($string1);
                $lengte2 = strlen($string2);                
                
                return($lengte1 == $lengte2);
            }
            /* De de testen strings */ 
            $woord1 = "dit is een tekst,ook"; 
            $woord2 = "dit is ook een tekst";
            
            if(evenLang($woord1, $woord2))print("Deze strings zijn even lang");
            else print "Deze strings zijn niet even lang";
            
            echo '<br><hr><br>';
            /* De breekaf functiie 
            Schrijf een eigen functie die als parameter een string accepteert. De functie heeft als naam "breekaf" en geeft de eerste 3 letters van de string terug, gevolgd door 3 puntjes. Als je dus "Webleren is fun" als parameter zou meegeven, zou de functie de string "Web..." teruggeven.

            Je mag ervan uit gaan dat de string altijd minstens 3 tekens bevat. Om te testen schrijf je volgende code in de body van je pagina.
            ******************************************************************************************/
            
            /* eigen methode */ 
            function breefkzelfaf($string){
                $woord = str_split($string, 3);           
                $puntjes = "...";
                return $woord[0] . $puntjes;
            }
            
            /*methode VDAB (oplossing) */ 
            function breekVDABaf($tekst) {
                $deeltekst = substr($tekst, 0, 3) . "...";
                return($deeltekst);
            }
            
            
            print("Webleren is fun --- " . breefkzelfaf("Webleren is fun") . "<br>");
            print("VDAB --- " . breekVDABaf("VDAB"));
            

            /* nieuwe les 
            ************************************************************/ 
            function nieuweLes(){
                return print('<br><hr><br>');
            }
            
            function enter(){
                return print ('<br><br>');
            }
            
            function softEnter(){
                return print ('<br>');
            }
            
            nieuweLes();

            /* Globale variabelen 
            Als je onderstaande code uitvoert zal je merken dat er foutmeldingen verschijnen. De reden hiervan is dat eerst en vooral de functie som() niet kan uitgevoerd worden omdat de variabelen $var1 en $var2 niet toegankelijk zijn in de functie.
            
            Door de lokale variabelen om te vormen tot globale variabelen kan je ervoor zorgen dat het voorbeeld van de theorie pagina toch correct zal uitgevoerd worden. Hoe je dit doet zie je in het onderstaand praktijkvoorbeeld.
            *****************************************/
            
            $var1 = 5;
            $var2 = 2;
            
            
            function som() {
                global $var1; //
                global $var2;
                global $resultaat;
                $resultaat = $var1 + $var2;
            }

            som();
            print("$resultaat");
            
            enter();
            
            function verhogen(&$var){
                $var++;
            }

            $a=5;
            print($a);
                
            softEnter();
            
            verhogen($a);
            print $a;
            // $a heeft nu de waarde 6
            
            nieuweLes();
           
            /* Include 
            	
            De functie require() is bijna identiek aan de functie include(). Het enige verschil is de behandeling van fouten als het in te voegen bestand niet kan gevonden worden. De functie include() zal in dat geval een foutmelding tonen en de rest van de code afwerken. Bij de functie require() zal er echter een fatale fout optreden en zal de rest van de code niet meer worden uitgevoerd.
            *****************************/
            
            /* Rekenkundige functies */ 
            echo round(5.146564,2);
            softEnter();
            echo ceil(5.146564); 
            softEnter();
            echo floor(5.146564);
            softEnter();
            echo sqrt(25) . " is de oplossing van de vierkantwortel van 25.";
            softEnter();
            echo pow(5,3) . " is de oplossing van 5 tot de 3de macht.";
            softEnter();
            echo rand(5,20) . " is een getal van 5 tot en met 20";
            softEnter();
echo openssl_random_pseudo_bytes(20);
            softEnter();
            echo random_bytes (20);
            softEnter();
            echo "Bovenstaande zijn 2 random bytes van 20 karakters";
            softEnter();
            echo abs(-2.34) . " is de absolute waarde van -2.34"; 
            softEnter();
            echo "De minimale waarde van 1, 5 en 3 is " . min(1, 5, 3) . " en de maximale waarde is " . max(1,5,3);
            softEnter();
                        
            ?>
            <a target="_blank" href="https://secure.php.net/manual/en/book.math.php">Meer winkunde php formules</a>

            <?php
            nieuweLes();
            /* Tijd verstreken sinds 
            1970 00:00:00 GMT. 
            **************************/
            
            echo time()/(60*60*24) . " dagen verstreken sinds 1970 00:00:00 GMT <br>Of ook wel " . floor(time()/(60*60*24)) . " volledige dagen.<br>" . floor(floor(time()/(60*60*24))/365.25) . " jaren zijn er dus reeds verstreken sinds 1970.";
            
            softEnter();
            echo date('D d-m-Y');
            
            softEnter();
            
            echo mktime(0,0,0,12,31,1970) . " is 1 jaar in seconden.";
            
            softEnter();
            echo ('<a href="https://secure.php.net/manual/en/function.date.php">Tijds notatie php</a><br>');
            
            nieuweLes();
            /* Tijd verlopen sinds m'n geboortedatum? */
            $nu = time();
            $geboortedatum = mktime(0,0,0,8,16,1990);
            $verschil = $nu - $geboortedatum;
            $verschilInDagen = ceil($verschil/(60*60*24));
            $verschilInJaren = floor($verschilInDagen/365.25);
            echo "Je bent geboren op " . date('d-m-Y', $geboortedatum). ".<br>Dat betekent dat je al " . $verschilInDagen . " dagen op deze wereld rondloopt.<br>Ook wel " .$verschilInJaren . " jaren." ; 
            
            enter();
            
            $nu = time();
            // We bepalen het huidige tijdstip in seconden.
//            $nu = mktime(19,59,59,5,15,2014);

            $geldigheid = mktime(20,0,0,5,15,2014);
            // We bepalen de tijd in seconden op 15 mei 2014 20:00:00.

            if ($nu < $geldigheid) print("Je bent op tijd.");
            else print("Je bent te laat, we hadden je verwacht voor " . date("d-m-Y H:i", $geldigheid));
            // We tonen de gepaste melding op het scherm afhankelijk dat het tijdstip is gepasseerd of niet.
            
            nieuweLes();
            
            /* ARRAYS 
            *******************************************/
            
            /* met variabelen */ 
            $dag1 = "maandag";
            $dag2 = "dinsdag";
            $dag3 = "woensdag";
            $dag4 = "donderdag";
            $dag5 = "vrijdag";
            $dag6 = "zaterdag";
            $dag7 = "zondag";

            print("$dag1 $dag2 $dag3 $dag4 $dag5 $dag6 $dag7");
            
            softEnter();
            
            /* met arrays */ 
            $dag = array("maandag","dinsdag","woensdag", "donderdag","vrijdag","zaterdag","zondag");
            
            // Methode 2
//         $dag[0] = "maandag";
// $dag[1] = "dinsdag";
// $dag[2] = "woensdag";
// $dag[3] = "donderdag";
// $dag[4] = "vrijdag";
// $dag[5] = "zaterdag";
// $dag[6] = "zondag";
            
            //We verwijderen maandag
//            unset($dag[0]);
            
            //We veranderen maandag in beikes
            $dag[0] = "Beikes, maandag";

            for($teller=0;$teller<=6;$teller++)
                print("$dag[$teller] ");
            
            /* Schrijf een programma dat de jaargetijden in de array $seizoenen opslaat. De index van elk element is de beginletter van het seizoen (dus Z voor zomer, H voor herfst, enz.).

            Het programma beslaat slechts één regel code als men de PHP begin- en eindtags niet meetelt.

            Opmerking: Aangezien je het resultaat niet naar het scherm moet schrijven zal je een lege pagina te zien krijgen als je ze opent in je browser. 
            *******************************/
            enter();
            
           $seizoenen = array ("Z" => "zomer", "H" => "herfst", "L" => "lente", "W" => "winter");
            
            echo $seizoenen["W"];
            softEnter();
            
            $letter = "Z";
            
            echo  $letter . " is van " . $seizoenen[$letter]; 
            softEnter();
            
            /* for each $dag 
            Pointermanipulatie
            De foreach-constructie            
            *************************************/ 
            
            for ($i = 0; $i < count($dag); $i++) {
                print("$dag[$i] ");
            }
            
            softEnter();
            print_r($dag);
            
            enter();
            
            /* manipulatie */
            $fruit = array("banaan", "appelsien");
            array_push($fruit, "appel", "peer");
            
            for ($i = 0; $i < count($fruit); $i++) {
                print("$fruit[$i] ");
                echo current($fruit) . "(<--pointer) <br>";
                
            }
            softEnter();
            print_r($fruit);
            
            
            /* 
            array_push
            Voeg nieuwe elementen toe aan het einde van de array.

            array_pop
            Verwijder het element aan het einde van de array.

            array_shift
            Verwijder het eerste element van de array.

            array_unshift
            Voeg elementen aan het begin van de array toe.

            array_multisort
            Met behulp van deze functie kan je een array alfabetisch rangschikken. De sorteervolgorde is een optionele parameter. Indien je SORT_ASC gebruikt zal je array in oplopende volgorde gesorteerd worden en als je voor SORT_DESC kiest zal hij in aflopende volgorde gesorteerd worden. 
            *******************/
            
            
            /* Een andere mogelijkheid om een array te overlopen is door gebruik te maken van de foreach-constructie. De syntaxis van deze constructie is de volgende:

            foreach($naamarray as $sleutel=>$inhoud) opdracht;
            *********************/
            $reeks = array ('België' => 'Brussel', 'Frankrijk' => 'Parijs', 'land' => 'hoofdstad', '...'=> '...' );
            ?>

            <table class="table table-condensed table-hover">
                <thead>
                    <tr>
                        <th>Land</th>
                        <th>Hoofdsatd</th>
                    </tr>
                </thead>
                <tbody>

                    <?php  
foreach ($reeks as $land => $hoofdstad) {
                ?>
                    <tr>
                        <td>
                            <?php print ($land); ?>
                        </td>
                        <td>
                            <?php print ($hoofdstad); ?>
                        </td>
                    </tr>

                    <?php 
                }
                
        ?>
                </tbody>
            </table>'

            <?php 
            
            nieuweLes();
            /* Multidimensionele arrays 
            $mijnarray[$index1][$index2] = waarde;
            ***************************/
            $dieren = array('zoogdieren'=>array('paard', 'koe', 'varken'), 'vogels'=>array('mus', 'merel'), 'insekten'=>array('vlieg', 'mug'));
            
            echo $dieren["zoogdieren"][1]; //koe! 
            softEnter();
            
            $letters = array(array('D', 'B'), array('A'), array(array('C', 'E')), 'F');
        
            echo $letters[2][0][1];
        
        
            ?>
        </p>

    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

    <script>

    </script>
</body>

</html>
