<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!--    elke seconde herladen-->
    <meta http-equiv="refresh" content="1">
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <title>Minimum Bootstrap HTML Skeleton</title>

</head>

<body>
    <?php 
    $tijdInSeconden = date("s"); 
    
    if($tijdInSeconden % 2 == 0) { // deelbaar door 2 == true
        echo("Het huidig aantal seconden (" . $tijdInSeconden . ") is even.<br><br>");
    } else { // Niet deelbaar door 2
        echo("Het huidig aantal seconden (" . $tijdInSeconden . ") is oneven. <br><br>");
    }
    
    print("Het aantal seconden is ");

    if (date("s") % 2 != 0) print("on");
    /* Als je het aantal seconden deelt door 2 en er is een rest, dan is het aantal seconden oneven. In dat geval schrijf je "on" naar het scherm. */

    print("even.");
    
    ?>



    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

</body>

</html>
