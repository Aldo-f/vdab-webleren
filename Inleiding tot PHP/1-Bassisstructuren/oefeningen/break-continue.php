<!DOCTYPE html>
<html lang="">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Title Page</title>

    <!-- Bootstrap CSS -->
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn t work if you view the page via file:// -->
    <!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
</head>

<body>


    <div class="jumbotron">

        <?php
        
        $getal1= 3;
        $getal2 = 5;
        $teller= 1;
        $totEnMet = 500;
        
        echo('<h1 class="display-3">Volgende getallen zijn deelbaar door '. $getal1 . ' en '. $getal2 . ', met startgetal ' . $teller .'</h1>');
        
        echo('<p class="lead">');
        echo('<div class="row">');



        echo('<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">');
        echo("<div class='h2'>Methode 1</div>");
        
        while (true) {
            $teller++;
            if ($teller % $getal1 == 0) {
                //true deelbaar door 3
                if ($teller % $getal2 == 0) {
                //true deelbaar door 5 (en 3)
                    if ($teller > $totEnMet) break;
                    print ($teller . " ");
                }
            }
        }
        echo('</div>');

        
        echo('<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">');
        echo("<div class='h2'>Methode 2</div>");
        
        $teller = 0;

        while (true) {
            $teller++;
            if (($teller % $getal1 <> 0) || ($teller % $getal2 <> 0)) continue;
            /* Als de teller gedeeld door 3 of de teller gedeeld door 5 een rest heeft die verschilt van 0 wordt de rest van de lus overgeslagen. */
            if ($teller > $totEnMet) break;
            /* Als de teller groter wordt dan 100 wordt de lus volledig afgebroken. */
            print($teller . " ");
        }
        
        echo('</p>');
        echo('</div>');
       echo( '</div>');

        
        
        ?>

    </div>


    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</body>

</html>
