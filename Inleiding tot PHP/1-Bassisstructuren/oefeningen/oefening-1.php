<!DOCTYPE html>
<html lang="">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Title Page</title>

    <!-- Bootstrap CSS -->
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn t work if you view the page via file:// -->
    <!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
</head>

<body>
    <div class="jumbotron">
        <h1 class="display-3">Oefening 1</h1>
        <hr class="m-y-2">
        <p class="lead">
            <?php
                $abc = "abc";
                $xyz = "xyz";
            ?>

            <div class="row">
                <div class="col-sm-6 col-md-3">
                    <div class="h2">Regel 1</div>

                    <p>
                        <?php
                        $counter = 0;
                        while ($counter <= 9){
                            echo($abc . "(" . $counter . ') ');
                            $counter++;
                            if ($counter >=9) break;
                        }
                        ?>

                    </p>

                </div>
                <div class="col-sm-6 col-md-3">
                    <div class="h2">Regel 2</div>
                    <p>
                        <?php
                        $counter = 0;
                        do { 
                            print($xyz . '(' .$counter.') ' ); 
                            $counter++;
                        }
                        while ($counter <= 8);
                       
                        ?>
                    </p>
                </div>
                <div class="col-sm-6 col-md-3">
                    <div class="h2">Regel 3</div>
                    <?php
                        for ($i = 1; $i <= 9; $i++) {
                            echo $i . ' ';
                        }                       
                    ?>

                </div>
                <div class="col-sm-6 col-md-3">
                    <div class="h2">Regel 4</div>

                    <?php
                        $alfabet = range ('A', 'F');
                    
                        echo("<ol>");
                    
                        for ($i = 0; $i <= 5; $i++) {
                            
                            echo("<li>");
                            echo("\"Item ");
                            echo $alfabet[$i] . '",  ';
                            echo("</li>");
                        }  
                    
                     echo("</ol>");
                    ?>
                </div>
            </div>
        </p>

        <h1 class="display-3">Oefening 2</h1>
        <hr class="m-y-2">
        <p class="lead">
            <?php
            
            $i= 0;
            $rijen = 20;
            
            for($i ; $i <= $rijen; $i++){
                echo("<p>");
                for($kolom = 1 ;$kolom <= $i; $kolom++){
                    echo("* ");
                }
                echo("</p>");
                
            }
            
            
            
            ?>
        </p>

    </div>


    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</body>

</html>
