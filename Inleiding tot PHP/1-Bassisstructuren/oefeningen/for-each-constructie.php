<!DOCTYPE html>
<html lang="">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Title Page</title>

    <!-- Bootstrap CSS -->
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn t work if you view the page via file:// -->
    <!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
    <style>
        body,
        html {
            height: 100%;
        }

    </style>
</head>

<body>
    <div class="container h-100">

        <div class="row h-100 justify-content-center align-items-center">
            <div class="row bg-faded jumbotron">
                <h1 class="display-3">Stukken fruit en hun kleuren</h1>
                <hr class="m-y-2">


                <?php 
                $reeks = array ("appel" => "rood", "peer" => "groen", "sinaasappel" => "oranje", "banaan" => "geel");
                //print_r ($reeks);
                ?>
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                    <h4>Methode 1</h4>
                    <p>Met de foreach-constructie</p>
                    <ul>
                        <?php 
                    
                    foreach ($reeks as $fruit => $kleur) { ?>
                        <li>Een
                            <?php echo $fruit ?> is
                            <?php echo $kleur ?>
                        </li>
                        <?php } ?>
                    </ul>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                    <h4>Methode 2</h4>
                    <p>Met pointermanipulatie </p>
                    <ul>
                        <?php
                        for($i = 0; $i < count($reeks); $i++) {
                            $fruit = key($reeks);
                            $kleur = current($reeks);
                        ?>
                        <li>
                            <?php print("Een $fruit is $kleur."); ?>
                        </li>
                        <?php next($reeks);
                        } ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>


    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</body>

</html>
