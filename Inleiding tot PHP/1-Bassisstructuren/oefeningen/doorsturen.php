<!--
Maak een pagina doorsturen.php die de gebruiker doorstuurt naar verschillende websites, afhankelijk van het getal gegenereerd door een random generator. Deze genereert een willekeurig getal tussen een zekere onder- en bovengrens. Gebruik als ondergrens 1 en als bovengrens 10. Is het gegenereerde getal even, dan wordt de bezoeker doorgestuurd naar http://www.vdab.be. Is het getal oneven, dan komt de bezoeker op http://www.google.be terecht.

Hint: Een willekeurig getal genereren kan met de functie rand(). Als eerste parameter kan je een ondergrens meegeven en als tweede parameter een bovengrens.

Je oplossing testen kan eenvoudig door een aantal keren doorsturen.php op te roepen. Soms kom je dan terecht op de Google-zoekmachine, soms op de VDAB website.-->

<?php 
    $randomGetal = rand(1,10);
//    $randomGetal = 1;
    
//    echo $randomGetal;

    //Als gegenereerde getal is even
    if ($randomGetal % 2 == 0 ) {
//        echo " is even";
        header("Location: https://vdab.be");
    }
    else 
//        echo " is oneven";
        header("Location: https://google.be");


?>
