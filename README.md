<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">

# Certificaten & Werkmappen online cursussen 

In deze git komen in principe alle cursussen die ik volgde, welke al dan niet voltooid werden.
Evenals het bijhorend certificaat kan hier geraadpleegd worden. 



### Websites die ik - al dan niet met hulp - creëerde:
* [www.aldofieuw.com](https://www.aldofieuw.com)
* [www.degrizzly.be](https://www.degrizzly.be)
* [www.vermistekatten.be](https://www.vermistekatten.be)
* [www.elsandtheartists.be](https://www.elsandtheartists.be)
* [www.koopjes.lottejespers.be](https://www.koopjes.lottejespers.be)
* ... 



### Oplossingen/werkmappen van cursussen:
* Inleiding tot PHP
* BTW van A tot Z
* Microsoft Excel 2010
* Websites ontwerpen met HTML5 

### Behaalde certificaten: 
* Basiskennis bedrijfsbeheer
* CVO KISP (ASP.NET, MVC, jQuery, ...)
 * Serverside-scripting 1 & 2
 * Webanimatie 1 & 2
* Online Marketing via Google Digitaal Atelier
* Android Basics Nanodegree by Google
* VDAB
 * Webdesign - Stylesheets in CSS3
 * Websites ontwerpen met HTMML5
 * Sociale media op de werkvloer
 * MS Excel 2010
 * MS Word 2013
 * Adobe Dreamweaver: basis
 * Animaties maken met Camtasia Studio
 


#### Hier ben ik te vinden:
* [t.me/aldofieuw](t.me/aldofieuw)
* [gravatar.com/aldofieuw](https://nl.gravatar.com/aldofieuw)
* [linkedin.com/in/aldofieuw](https://www.linkedin.com/in/aldofieuw/)
* [gitlab.com/Aldo-f](https://gitlab.com/Aldo-f/)





 
<sup><i class="fa fa-coffee"></i> [Koop me een koffie](https://paypal.me/AldoF)</sup>
